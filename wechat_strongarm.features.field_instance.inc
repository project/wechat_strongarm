<?php
/**
 * @file
 * wechat_strongarm.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function wechat_strongarm_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'wechat_reply-wechat_strongarm_news-field_wechat_strongarm_data'.
  $field_instances['wechat_reply-wechat_strongarm_news-field_wechat_strongarm_data'] = array(
    'bundle' => 'wechat_strongarm_news',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'wechat_reply',
    'field_name' => 'field_wechat_strongarm_data',
    'label' => 'Wechat strongarm data',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => -6,
    ),
  );

  // Exported field_instance:
  // 'wechat_reply-wechat_strongarm_reply-field_wechat_strongarm_data'.
  $field_instances['wechat_reply-wechat_strongarm_reply-field_wechat_strongarm_data'] = array(
    'bundle' => 'wechat_strongarm_reply',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'wechat_reply',
    'field_name' => 'field_wechat_strongarm_data',
    'label' => 'Wechat strongarm data',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => -5,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Wechat strongarm data');

  return $field_instances;
}
