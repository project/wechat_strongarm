<?php

/* 
 * wechat strongarm admin setting form
 */
function wechat_strongarm_config_form($form, $form_state) {
  
  $form['wechat_strongarm_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable cron to rebuild cache'),
    '#description' => t('通过 cron重建缓存，此处的url 设置会覆盖默认的缓存策略，另外注意要运行 cronjob，间隔不能大于此处设置的时间间隔。'),
    '#default_value' => variable_get('wechat_strongarm_cron', 0),
  );
  
  $form['wechat_strongarm_cron_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Interval to rebuild cache'),
    '#description' => t('单位：小时。比如6，表示6小时。'),
    '#default_value' => variable_get('wechat_strongarm_cron_interval', 2),
    '#states' => array( 
      'visible' => array(
        'input[name="wechat_strongarm_cron"]' => array('checked' => TRUE)
      )
     )
  );
  
  $form['wechat_strongarm_cron_urls'] = array(
    '#type' => 'textarea',
    '#title' => t('The urls to be cached'),
    '#description' => t('每行一个url。注意：这里的缓存策略会覆盖默认的缓存设置。'),
    '#default_value' => variable_get('wechat_strongarm_cron_urls', 2),
    '#states' => array( 
      'visible' => array(
        'input[name="wechat_strongarm_cron"]' => array('checked' => TRUE)
      )
     )
  );
  
  return system_settings_form($form);
}
