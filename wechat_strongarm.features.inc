<?php
/**
 * @file
 * wechat_strongarm.features.inc
 */

/**
 * Implements hook_default_wechat_reply_type().
 */
function wechat_strongarm_default_wechat_reply_type() {
  $items = array();
  $items['wechat_strongarm_news'] = entity_import('wechat_reply_type', '{
    "type" : "wechat_strongarm_news",
    "label" : "Wechat strongarm news",
    "weight" : "0",
    "description" : "",
    "data" : null,
    "rdf_mapping" : []
  }');
  $items['wechat_strongarm_reply'] = entity_import('wechat_reply_type', '{
    "type" : "wechat_strongarm_reply",
    "label" : "Wechat strongarm reply",
    "weight" : "0",
    "description" : "",
    "data" : null,
    "rdf_mapping" : []
  }');
  return $items;
}
