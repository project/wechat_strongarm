<?php
/**
 * @file
 * wechat_strongarm.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function wechat_strongarm_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_wechat_strongarm_data'.
  $field_bases['field_wechat_strongarm_data'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_wechat_strongarm_data',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text_long',
  );

  return $field_bases;
}
